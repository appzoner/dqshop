import React from "react";
import 'bootswatch/dist/lux/bootstrap.css';

class Layout extends React.Component<{ title: string, description: string, children: any }> {
	render() {
		const { title, description, children } = this.props;
		
		return (
			<>
				<title> { title ? title : "Homepage" }</title>
				<meta name="description" content={description || "DQSHOP"} />
				<main className="container">
					{ children }
				</main>
			</>
		)
	}
}

export default Layout;