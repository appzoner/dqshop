import React from "react";
import Layout from "../../components/Layout";

class Store extends React.Component {
	render() {
		return (
			<Layout title="DQSHOP - Store Page" description="This is dqshop store page">
				<div>
					<div className="text-center mt-5">
						<h1>Store</h1>
						<p>This is store page</p>
					</div>
				</div>
			</Layout>
		)
	}
}

export default Store;